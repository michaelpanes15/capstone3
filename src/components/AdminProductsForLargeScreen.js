import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import EditProduct from './EditProduct.js';
import ArchiveProduct from './ArchiveProduct.js';

export default function AdminProductsForLargeScreen({productsData, fetchData}) {
  const [product, setProduct] = useState([]);

  useEffect(() => {
    const mappedProduct = productsData.map((product, index) => (
      <tr key={index}>
        <td className="bg-dark card-text">{product._id}</td>
        <td className="bg-dark card-text">{product.name}</td>
        <td className="bg-dark card-text">{product.description}</td>
        <td className="bg-dark card-text"><span>&#8369;</span> {product.price}</td>
        <td className={(product.isActive) ? "bg-dark card-text" : "text-danger bg-dark card-text"}>
         {(product.isActive) ? "Available" : "Unavailable"}
        </td>
        <td className="bg-dark card-text"><EditProduct product={product._id} fetchData={fetchData}/></td>
        <td className="bg-dark card-text"><ArchiveProduct product={product._id} fetchData={fetchData} isActive={(product.isActive)}/></td>
      </tr>
    ));

    setProduct(mappedProduct);
  }, [productsData, fetchData]);

  return (
    <>
      <Container>
        <div>
          <Table className="table table-striped table-bordered table-responsive">
            <thead>
              <tr className="text-center">
                <th className="bg-dark card-text">ID</th>
                <th className="bg-dark card-text">Name</th>
                <th className="bg-dark card-text">Description</th>
                <th className="bg-dark card-text">Price</th>
                <th className="bg-dark card-text">Availability</th>
                <th className="bg-dark card-text" colSpan="3">Actions</th>
              </tr>
            </thead>
            <tbody>
              {product}
            </tbody>
          </Table>
        </div>
      </Container>
    </>
  );
}
