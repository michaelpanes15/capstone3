import React, { useState } from 'react';
import Swal from 'sweetalert2';
import '../App.css'

const UpdateProfile = ({user, fetchData}) => {
  const [userName, setUserName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  
  const handleSubmit = async (e) => {
    e.preventDefault();

    // Show a confirmation modal using Swal
    const confirmResult = await Swal.fire({
      title: 'Confirm Update Profile',
      text: 'Are you sure you want to update your profile?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes, update it',
      cancelButtonText: 'Cancel',
    });

    // Check the user's choice
    if (!confirmResult.isConfirmed) {
      // The user canceled, do nothing
      return;
    }

    const data = {
      id: user.id,
      userName: userName,
      mobileNo: mobileNo
    };

    try {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
      })
      .then(res => res.json())
      .then(result => {
        if(result.status === true){
            Swal.fire({
                title: "Success!",
                icon: "success",
                text: "Update User Profile Success"
              })
            fetchData();
            setUserName("");
            setMobileNo("");
          } else {
            Swal.fire({
                title: "Failed!",
                icon: "error",
                text: "Please try again"
              })
          }

      })
    } catch (error) {
      //console.error('An error occurred:', error);
    }
  };

  return (
    <div className="container">
      <h2>Update Profile</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group mb-3">
          <label className="form-label">Username</label>
          <input
            type="text"
            className="form-control"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="form-group mb-3">
          <label className="form-label">Mobile Number</label>
          <input
            type="text"
            className="form-control"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          />
        </div>
        <button type="submit" className="btn banner-button banner-button-custom">
          Update Profile
        </button>
      </form>
    </div>
  );
};

export default UpdateProfile;