import { useState } from 'react';
import AdminNavBar from './AdminNavBar';
import AdminProduct from './AdminProducts.js';
import AdminUsers from './AdminUsers.js';
import AdminOrders from './AdminOrders.js';

export default function AdminView({productsData, fetchData, isAdmin}) {
  const [isProducts, setIsProducts] = useState(true);
  const [isUsers, setIsUsers] = useState(false);
  const [isOrders, setIsOrders] = useState(false);

  return (
    <>
      <AdminNavBar isProducts={isProducts} setIsProducts={setIsProducts} isUsers={isUsers} setIsUsers={setIsUsers} isOrders={isOrders} setIsOrders={setIsOrders}/>
      <AdminProduct isProducts={isProducts} productsData={productsData} fetchData={fetchData} />
      <AdminUsers isUsers={isUsers} fetchData={fetchData} />
      <AdminOrders isOrders={isOrders} isAdmin={isAdmin}/>
    </>
  );
}
