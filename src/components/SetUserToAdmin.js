import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function SetUserToAdmin({user, fetchData}) {

	const upgradeUser = (userId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/set-admin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({id: userId})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			if(data) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "User Admitted to Admin Status"
				})
				fetchData();
			}
			else {
				Swal.fire({
					title: "Failed!",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	return(
		<Button className="banner-button banner-button-custom" size="sm" onClick={() => upgradeUser(user._id)} disabled={user.isAdmin === true}>Set to Admin</Button>
	)
}