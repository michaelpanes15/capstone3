import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function EditProduct({product, fetchData}) {
	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	// state for editCourse Modals to open/close
	const [showEdit, setShowEdit] = useState(false);

	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
	}

	const editProduct = (e, productId) => {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Product updated successfully"
				})
				closeEdit();
				fetchData();
			}
			else {
				Swal.fire({
					title: "Failed!",
					icon: "error",
					text: "Please try again"
				})
				closeEdit();
			}
		})
	}

	return(
		<>
			<Button className="banner-button banner-button-custom" size="sm" onClick={() => openEdit(product)}>Edit</Button>

			<Modal show={showEdit} onHide={closeEdit} >
				<Form onSubmit = {e => editProduct(e, productId)}>
					<Modal.Header className="bg-black card-text" closeButton>
						<Modal.Title >Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body className="bg-black card-text">
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control 
				                type="text"
				                value = {name}
				                onChange = {event => {setName(event.target.value)}}
				                required
			                />
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
				                as="textarea"
				                rows={3}
				                value = {description}
				                onChange = {event => {setDescription(event.target.value)}}
				                required
			                />
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
				                type="number"
				                value = {price}
				                onChange = {event => {setPrice(event.target.value)}}
				                required
			                />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer className="bg-black card-text">
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button className="banner-button banner-button-custom" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}