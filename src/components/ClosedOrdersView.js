import {useEffect, useState } from 'react';
import {Container} from 'react-bootstrap';
import OrdersCard from './OrdersCard.js';

export default function ClosedOrdersView({isClosed, orders}) {
	const [closedOrder, setClosedOrder] = useState([]);
	const [productsList, setProductsList] = useState([]);

	useEffect(() => {
		// Filter pending orders and set them in the state
	    setClosedOrder(orders);
		const filteredClosedOrders = orders;
		let orderIndex = 0;
		setProductsList(filteredClosedOrders.map((order) => {
			return (
				<OrdersCard key={order._id} orders={order} orderIndex={orderIndex++}/>
			)
		}))
	}, [orders, closedOrder])

	return (
		<>
		<Container hidden={isClosed===false}>
			{
			orders.length === 0 ? (
			    <div className="text-center mt-3 mb-3">
		          <p className="fs-5 header-title">No Closed Orders</p>
		        </div>
			  ) : (
			    productsList // Render the list of products here
			  )
			}
		</Container>
		</>
	)
}