import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom';

export default function Banner({data}){
	const {title, description, destination, label} = data;
	return (
		<>
			<Row>
				<Col className="p-3 text-center banner">
					<div className="bg-dark p-3 card-form">
						<h1>{title}</h1>
						<p>{description}</p>
						<Link to={destination}>
							<Button className="banner-button banner-button-custom">{label}</Button>
						</Link>
					</div>
				</Col>
			</Row>
		</>

	)
} 