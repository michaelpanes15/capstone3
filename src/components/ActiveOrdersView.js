import {useEffect, useState } from 'react';
import {Container} from 'react-bootstrap';
import OrdersCard from './OrdersCard.js';

export default function ActiveOrdersView({isActive, orders}) {
	const [activeOrder, setActiveOrder] = useState([]);
	const [productsList, setProductsList] = useState([]);

	useEffect(() => {
		// Filter pending orders and set them in the state
	    setActiveOrder(orders);
        const filteredActiveOrders = orders;
        let orderIndex = 0;
        setProductsList(
          filteredActiveOrders.map((order) => (
            <OrdersCard key={order._id} orders={order} orderIndex={orderIndex++} />
          ))
        );
	}, [orders, activeOrder])

	return (
		<>
		<Container hidden={isActive===false}>
			{
			orders.length === 0 ? (
			    <div className="text-center mt-3 mb-3">
		          <p className="fs-5 header-title">No Active Orders</p>
		        </div>
			  ) : (
			    productsList // Render the list of products here
			  )
			}
		</Container>
		</>
	)
}