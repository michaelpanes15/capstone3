import { useState, useEffect } from 'react';
import AdminUsersForLargeScreen from './AdminUsersForLargeScreen.js';
import AdminUsersForSmallScreen from './AdminUsersForSmallScreen.js';
import '../App.css'

export default function AdminUsers({isUsers, fetchData}) {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details/all`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(result => {
      if(result.status){
        setUsers(result.message)
      }
    })
  }, [fetchData]);

  return (
    
    <div hidden={isUsers===false}>
      {/* Show different content based on screen size */}
      <div className="large-screen-content">
        <AdminUsersForLargeScreen users={users} fetchData={fetchData}/>
      </div>
      <div className="small-screen-content">
        <AdminUsersForSmallScreen users={users} fetchData={fetchData}/>
      </div>
    </div>
  );
}
