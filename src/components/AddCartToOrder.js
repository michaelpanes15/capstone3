import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap'

export default function AddCartToOrder({updateTotalPrice, product, fetchData, orderList}) {
	const [isActive, setIsActive] = useState(false);

	const addToList = (product) => {
		orderList.push(product);
		setIsActive(true);
		updateTotalPrice();
		//console.log(orderList);
	}

	const removeFromList = (product) => {
		orderList.splice(orderList.indexOf(product), 1);
		setIsActive(false);
		updateTotalPrice();
		//console.log(orderList);
	}

	useEffect(() => {
		if(orderList.some((orderItem) => orderItem.name === product.name)) setIsActive(true);
		else setIsActive(false);
		//console.log(orderList);
	}, [orderList, product])

	return(
		(isActive) ?
			<Button className="cart-button" variant="danger" size="sm" onClick={() => removeFromList(product)}>Remove</Button>
		:
			<Button className="banner-button banner-button-custom cart-button" size="sm" onClick={() => addToList(product)}>Add</Button>
	)
}