import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

export default function PreviewProducts(props) {
	const {breakPoint, data} = props;
	const {id, name, price, imageUrl} = data;

	return (
		<Col xs={6} md={4} lg={breakPoint} className="mb-3 ">
			<Link to={`/products/${id}`} className="product-link">
				<Card className="bg-dark card-text">
					<Card.Img className="p-1 img-fluid w-100 mx-auto" variant="top" src={imageUrl} />
					<Card.Body>
						<Card.Title className="text-left truncate-text-two-lines">
							{name}
						</Card.Title>
					</Card.Body>
					<Card.Footer>
						<h5 className="text-left"><span>&#8369;</span> {price}</h5>
					</Card.Footer>
				</Card>
			</Link>
		</Col>
	)
}