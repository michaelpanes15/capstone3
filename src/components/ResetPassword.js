import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ResetPassword({user}) {
  const [oldPassword, setOldPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [message, setMessage] = useState('');
  const navigate = useNavigate();

  const handleResetPassword = async (e) => {
    e.preventDefault();

    // Show a confirmation modal using Swal
    const confirmResult = await Swal.fire({
      title: 'Confirm Password Reset',
      text: 'Are you sure you want to reset your password?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes, reset it',
      cancelButtonText: 'Cancel',
    });

    // Check the user's choice
    if (!confirmResult.isConfirmed) {
      // The user canceled, do nothing
      return;
    }
        
    if (password !== confirmPassword) {
      setMessage('Passwords do not match');
      return;
    }
    fetch(`${process.env.REACT_APP_API_URL}/users/reset-password`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
              id: user.id,
              oldPassword: oldPassword,
              newPassword: password
            })
          })
    .then(res => res.json())
    .then(data => {
      //console.log(data);
      if(data.status === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Reset password success"
          })
          navigate('/logout');
        }
        else {
          Swal.fire({
            title: "Failed!",
            icon: "error",
            text: "Please try again"
          })
        }
    })
  }

  return(
    <div className="container">
      <h2>Reset Password</h2>
      <form onSubmit={handleResetPassword}>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Old Password
          </label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={oldPassword}
            onChange={(e) => setOldPassword(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            New Password
          </label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="confirmPassword" className="form-label">
            Confirm Password
          </label>
          <input
            type="password"
            className="form-control"
            id="confirmPassword"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            required
          />
        </div>
        {message && <div className="alert alert-danger">{message}</div>}
        <button type="submit" className="btn banner-button banner-button-custom">
          Reset Password
        </button>
      </form>
    </div>    
  )
}