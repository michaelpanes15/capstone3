import {Nav, Navbar, Container} from 'react-bootstrap';
 
export default function AdminNavBar({isProducts, setIsProducts, isUsers, setIsUsers, isOrders, setIsOrders}) {

  // Function to handle link clicks and update the active state
  const handleLinkClick = (link) => {
    if (link === 'products') {
      setIsProducts(true);
      setIsUsers(false);
      setIsOrders(false);
    } else if (link === 'users') {
      setIsProducts(false);
      setIsUsers(true);
      setIsOrders(false);
    } else if (link === 'orders') {
      setIsProducts(false);
      setIsUsers(false);
      setIsOrders(true);
    }
  };

  return (
    <>
      <Navbar className="my-3" bg="dark rounded" variant="dark">
        <Container>
          <Nav className="w-100 justify-content-between">
            <Nav.Item className="ms-5">
              <Nav.Link
                className={`nav-link ${isProducts ? 'active' : ''}`}
                onClick={() => handleLinkClick('products')}
              >
                Products
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                className={`nav-link ${isUsers ? 'active' : ''}`}
                onClick={() => handleLinkClick('users')}
              >
                Users
              </Nav.Link>
            </Nav.Item>
            <Nav.Item className="me-5">
              <Nav.Link
                className={`nav-link ${isOrders ? 'active' : ''}`}
                onClick={() => handleLinkClick('orders')}
              >
                Orders
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}