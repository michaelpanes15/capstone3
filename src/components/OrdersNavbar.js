import {Nav, Navbar, Container} from 'react-bootstrap';
 
export default function OrdersNavbar({isPending, setIsPending, isActive, setIsActive, isClosed, setIsClosed}) {

  // Function to handle link clicks and update the active state
  const handleLinkClick = (link) => {
    if (link === 'pending') {
      setIsPending(true);
      setIsActive(false);
      setIsClosed(false);
    } else if (link === 'active') {
      setIsPending(false);
      setIsActive(true);
      setIsClosed(false);
    } else if (link === 'closed') {
      setIsPending(false);
      setIsActive(false);
      setIsClosed(true);
    }
  };

  return (
    <>
      <Navbar className="my-3" bg="dark rounded" variant="dark">
        <Container>
          <Nav className="w-100 justify-content-between">
            <Nav.Item className="ms-5">
              <Nav.Link
                className={`nav-link ${isPending ? 'active' : ''}`}
                onClick={() => handleLinkClick('pending')}
              >
                Pending Orders
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                className={`nav-link ${isActive ? 'active' : ''}`}
                onClick={() => handleLinkClick('active')}
              >
                Active Orders
              </Nav.Link>
            </Nav.Item>
            <Nav.Item className="me-5">
              <Nav.Link
                className={`nav-link ${isClosed ? 'active' : ''}`}
                onClick={() => handleLinkClick('closed')}
              >
                Closed Orders
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}