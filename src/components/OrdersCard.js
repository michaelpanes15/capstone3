import {useEffect, useState } from 'react';
import {Card, Accordion, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function OrdersCard({orders, orderIndex, isAdmin, fetchData}) {
	const [productsList, setProductsList] = useState([]);
	const [orderDetails, setOrderDetails] = useState('');
	const [dateTime, setDateTime] = useState('');

	const dateOptions = {
	  year: "numeric",
	  month: "long",
	  day: "numeric",
	  hour: "2-digit",
	  minute: "2-digit",
	  second: "2-digit",
	  timeZone: "Asia/Manila", // Specify the Philippines time zone
	};

	const closeOrder = () => {
		Swal.fire({
	      title: 'Are you sure you want to close this order?',
	      text: 'This action cannot be undone!',
	      icon: 'warning',
	      showCancelButton: true,
	      confirmButtonText: 'Yes, close it!',
	      cancelButtonText: 'No, cancel',
	    }).then((result) => {
	      if (result.isConfirmed) {
	        fetch(`${process.env.REACT_APP_API_URL}/orders/close`, {
	          method: 'PUT',
	          headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	          },
	          body: JSON.stringify({
	          	userId: `${orders.userId}`,
	          	orderId: `${orders._id}`
	          })
	        })
	        .then(res => res.json())
	        .then(result => {
	          if(result.status){
	        	Swal.fire({
          	      title: 'Order Closed!',
          	      text: 'Order was sucessfully been closed',
          	      icon: 'success'
          	    })
          	    fetchData();
	          }
	          else {
          		Swal.fire({
          	      title: 'Close Failed!',
          	      text: `${result.message}`,
          	      icon: 'error'
          	    })
	          }
	        })
	      }
	    });

	}

	useEffect(() => {
		let products = orders.products;
		setProductsList(products.map((product) => {
			return (
				<li key={product.productId} className="header-title">
	     	      {product.name} x {product.quantity}
	     	    </li>
			)
		}))

		if(orders.orderStatus !== "Pending" && orders.paymentDetails !== "" && orders.deliveryAddress !== ""){
			setOrderDetails(() => {
				return (
					<>
					<Card.Subtitle>Payment Method:</Card.Subtitle>
					<Card.Text>{orders.paymentDetails}</Card.Text>
					<Card.Subtitle>Delivery Address:</Card.Subtitle>
					<Card.Text>{orders.deliveryAddress}</Card.Text>
					</>
				)
			})
		}

		if(orders.purchasedOn !== undefined){
			const purchaseDate = new Date(orders.purchasedOn);
			setDateTime(purchaseDate.toLocaleString("en-US", dateOptions))
		}
		else if(orders.purchasedOn === undefined && orders.orderStatus === "Pending") setDateTime("Pending");
		else setDateTime("Cancelled");
		
		// eslint-disable-next-line
	}, [orders])

  return (
  	(!isAdmin && orders.orderStatus === "Pending")?
  	<Card className="my-3 bg-dark card-text">
	  <Card.Body >
	    <Card.Title className="">Order ID: {orders._id}</Card.Title>
	    
	    <Card.Subtitle className="">Orders:</Card.Subtitle>
	    	<ol>
	    		{productsList}
	    	</ol>
	    <Card.Subtitle className="">Total Price:</Card.Subtitle>
	    <Card.Text className=""><span>&#8369;</span> {orders.totalAmount}</Card.Text>
	  </Card.Body>
	</Card>

  	:
  	<>
  		<Accordion  defaultActiveKey={orderIndex.toString()} className="mb-2">
  	      <Accordion.Item eventKey={orderIndex.toString()}>
  	        <Accordion.Header className="bg-dark card-text">Order ID: {orders._id}</Accordion.Header>
  	        <Accordion.Body className="bg-dark card-text">
  	        	{
  	        		(isAdmin) ? 
  	        			<>
  	        			<Card.Subtitle>Ordered by:</Card.Subtitle>
  	        			<Card.Text>{orders.userId}</Card.Text>
  	        			<Card.Subtitle>Order Status:</Card.Subtitle>
  	        			<Card.Text>{orders.orderStatus}</Card.Text>
  	        			</>
  	        		:
  	        		<>
  	        		</>
  	        	}
  	          	<Card.Subtitle>Orders:</Card.Subtitle>
        	    	<ol>
        	    		{productsList}
        	    	</ol>
        	    <Card.Subtitle>Total Price:</Card.Subtitle>
        	    <Card.Text><span>&#8369;</span> {orders.totalAmount}</Card.Text>
        	    {orderDetails}
        	    <Card.Subtitle>Date Purchased:</Card.Subtitle>
        	    <Card.Text>{dateTime}</Card.Text>
        	    {

  	        		(isAdmin && orders.orderStatus !== "Closed") ? 
  	        			<>
  	        			 <Button className="banner-button banner-button-custom" onClick={closeOrder}>
		                   Close Order
		                 </Button>
  	        			</>
  	        		:
  	        		<>
  	        		</>
  	        	}
  	        </Accordion.Body>
  	      </Accordion.Item>
  	    </Accordion>
  	</>

  );
}

