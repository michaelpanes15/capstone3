import AddProduct from './AddProduct.js';
import AdminProductsForLargeScreen from './AdminProductsForLargeScreen.js';
import AdminProductsForSmallScreen from './AdminProductsForSmallScreen.js';
import '../App.css';

export default function AdminProduct({isProducts, productsData, fetchData}) {

  return (
    <div hidden={isProducts===false}>
      {/* Show different content based on screen size */}
      <div className="d-grid justify-content-center pb-3">
        <AddProduct fetchData={fetchData}/>
      </div>
      <div className="large-screen-content">
        <AdminProductsForLargeScreen isProducts={isProducts} productsData={productsData} fetchData={fetchData}/>
      </div>
      <div className="small-screen-content">
        <AdminProductsForSmallScreen isProducts={isProducts} productsData={productsData} fetchData={fetchData}/>
      </div>
    </div>
  );
}
