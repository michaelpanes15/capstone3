import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import SetUserToAdmin from './SetUserToAdmin.js';


export default function AdminUsersForSmallScreen({users, fetchData}) {
  const [usersList, setUsersList] = useState([]);

  useEffect(() => {
    const mappedUsers = users.map((user, index) => {
          return (
            <Table key={`${user._id}-${index}`} className="table table-striped table-bordered table-responsive">
              <tbody>
                <tr>
                  <td className="bg-dark card-text">User Id</td>
                  <td className="bg-dark card-text">{user._id}</td>
                </tr>
                <tr>
                  <td className="bg-dark card-text">Username</td>
                  <td className="bg-dark card-text">{user.userName}</td>
                </tr>
                <tr>
                  <td className="bg-dark card-text">Email</td>
                  <td className="bg-dark card-text">{user.email}</td>
                </tr>
                <tr>
                  <td className="bg-dark card-text">Mobile Number</td>
                  <td className="bg-dark card-text">{user.mobileNo}</td>
                </tr>
                <tr>
                  <td className="bg-dark card-text">User Permissions</td>
                  <td className="bg-dark card-text">{(user.isAdmin) ? "Admin" : "User"}</td>
                </tr>
                <tr>
                  <td className="bg-dark card-text">Set to Admin</td>
                  <td className="bg-dark card-text"><SetUserToAdmin user={user} fetchData={fetchData}/></td>
                </tr>
              </tbody>
            </Table>
          )
        })
    setUsersList(mappedUsers);
  }, [fetchData, users]);

  return (
    <>
      <Container >
        <div className="d-grid justify-content-center">
          {usersList}
        </div>
      </Container>
    </>
  );
}
