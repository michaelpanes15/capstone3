import { useState, useEffect} from 'react';
import { Row } from 'react-bootstrap';
import PreviewProducts from './PreviewProducts';

export default function FeaturedProducts(){
	const [previews, setPreviews] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {

			const numbers = [];
			const featured = [];

			const generateRandomNums = () => {
				let randomNum = Math.floor(Math.random() * data.length);
				if(numbers.indexOf(randomNum) === -1) {
					numbers.push(randomNum)
				}
				else {
					generateRandomNums();
				}
			}
			for(let i=0; i < ((data.length < 6) ? data.length: 6); i++){
				generateRandomNums();
				featured.push(
					<PreviewProducts data={data[numbers[i]]} key={i} breakPoint={2}/>
				)
			};
			setPreviews(featured);
		})
	}, [])

	return (
		<>
			<h2 className="text-center featured-title bg-dark p-3 card-form">Featured Products</h2>
			<Row className="justify-content-lg-center">
					{previews}
			</Row>
		</>
	)
}