import {useEffect, useState } from 'react';
import {Container} from 'react-bootstrap';
import OrdersCard from './OrdersCard.js';
import PendingActivate from './PendingActivate.js';

export default function PendingOrdersView({isPending, orders, fetchData}) {
	const [pendingOrder, setPendingOrder] = useState({});
	const [productsList, setProductsList] = useState([]);

	useEffect(() => {
		// Filter pending orders and set them in the state
	    setPendingOrder(orders);
		const filteredPendingOrders = orders;
		setProductsList(filteredPendingOrders.map((order) => {
			return (
				<OrdersCard key={order._id} orders={order}/>
			)
		}))
	    //console.log(pendingOrder);
	}, [orders, pendingOrder])
	return (
		
		<>
		<Container hidden={isPending===false}>
			{
			orders.length === 0 ? (
			    <div className="text-center mt-3 mb-3">
		          <p className="fs-5 header-title">No Pending Orders</p>
		        </div>
			  ) : (
			    <>
    				{productsList}
    				<PendingActivate pendingOrder={pendingOrder} fetchData={fetchData}/>
    			</> // Render the list of products here
			  )
			}
		</Container>
		</>
	)
}