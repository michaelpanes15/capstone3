import {useEffect, useState, useCallback } from 'react';
import {Button, Accordion, Container} from 'react-bootstrap';
import OrdersCard from './OrdersCard.js';

export default function AdminOrders({isOrders, isAdmin}) {
	const [orders, setOrders] = useState([]);
	const [ordersList, setOrdersList] = useState([]);
	const [isOrdersFirst, setIsOrdersFirst] = useState(true);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/admin/all`, {
		  method: 'GET',
		  headers: {
		    'Content-Type': 'application/json',
		    'Authorization': `Bearer ${localStorage.getItem('token')}`
		  }
		})
		.then(res => res.json())
		.then(result => {
		  if(result.status){
		    setOrders(result.message)
		  }
		})
	}

	const arrangeOrdersByDate = useCallback((orderList) => {
	    const ordersByDate = orderList.map((order_item, index) => {
	      return (
	        <OrdersCard key={index} orders={order_item} orderIndex={order_item._id} isAdmin={isAdmin} fetchData={fetchData}/>
	      );
	    })
	    setOrdersList(ordersByDate);
	  }, [isAdmin])

	  const arrangeOrdersByUser = useCallback((orderList) => {
	      const uniqueUserId = Array.from([...new Set(orderList.map(order => order.userId))]);
	      const userOrdersList = uniqueUserId.map((userId) => {
	        const userOrders = orderList.filter((order) => order.userId === userId).map((order_item) => (
	          <OrdersCard key={order_item._id} orders={order_item} orderIndex={order_item._id} isAdmin={isAdmin} fetchData={fetchData}/>
	        ));
	        // Add additional HTML content here if needed
	        return (
	          <div key={userId}>
	            {/* Additional HTML content */}
        		<Accordion  defaultActiveKey={userId.toString()} className="mb-2">
        	      <Accordion.Item eventKey={userId.toString()}>
        	        <Accordion.Header>User ID: {userId} </Accordion.Header>
        	        <Accordion.Body className="bg-dark card-text">
        	        	{userOrders}
        	        </Accordion.Body>
        	      </Accordion.Item>
        	    </Accordion>
	            
	          </div>
	        );
	      });

	      setOrdersList(userOrdersList);
	    },[isAdmin])

	  useEffect(() => {
	    fetchData();
	  }, []);

	  useEffect(() => {
	    if (isOrdersFirst) {
	      arrangeOrdersByDate(orders);
	      //setOrdersList(orders);
	    } else {
	      arrangeOrdersByUser(orders);
	      //setOrdersList(orders);
	    }
	  }, [orders, isOrdersFirst, arrangeOrdersByDate, arrangeOrdersByUser]);

	return (
		<>
		<Container hidden={isOrders===false}>
		<div className="d-flex justify-content-center mb-3">
			{
			(isOrdersFirst) ?
				<Button className="banner-button banner-button-custom" size="sm" onClick={() => setIsOrdersFirst(false)}>Sort by User</Button>
			:
				<Button className="banner-button banner-button-custom" size="sm" onClick={() => setIsOrdersFirst(true)}>Sort by Date </Button>
			}
		</div>
		<div>{ordersList}</div>
		</Container>
		</>
	)
}
