import { useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function AddProduct({fetchData}) {
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [imageUrl, setImageUrl] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);

	// state for editCourse Modals to open/close
	const [showEdit, setShowEdit] = useState(false);

	const openEdit = () => {
		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
	}

	const addProduct = (e) => {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				imageUrl: imageUrl
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "New Product Added"
				})
				closeEdit();
				fetchData();
			}
			else {
				Swal.fire({
					title: "Failed!",
					icon: "error",
					text: "Please try again"
				})
				closeEdit();
			}
		})
	}

	useEffect(() => {
		if(name !== "" && description !== "" && price !== "") setIsActive(false);
		else setIsActive(true);
	}, [name, description, price]);

	return(
		<>
			<Button className="justify-content-center banner-button banner-button-custom" variant="primary" size="sm" onClick={() => openEdit()}>Add Product</Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit = {e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>New Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control 
				                type="text"
				                value = {name}
				                onChange = {event => {setName(event.target.value)}}
				                required
			                />
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
				                as="textarea"
				                rows={3}
				                value = {description}
				                onChange = {event => {setDescription(event.target.value)}}
				                required
			                />
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
				                type="number"
				                value = {price}
				                onChange = {event => {setPrice(event.target.value)}}
				                required
			                />
						</Form.Group>
						<Form.Group controlId="productImageUrl">
							<Form.Label>Image</Form.Label>
							<Form.Control 
				                type="text"
				                value = {imageUrl}
				                onChange = {event => {setImageUrl(event.target.value)}} 
			                />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit" disabled={isActive === true}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}