import { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CheckoutOrder({isCheckoutActive, orderList, setOrderList, total, fetchData, updateOrderTotal, resetCart}) {
	const [isDisabled, setIsDisabled] = useState(true)

	const checkoutOrder = () => {
		Swal.fire({
		  title: 'Do you want to Checkout?',
		  text: `The total is PHP ${total}`,
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes'
		}).then((result) => {
			if(result.isConfirmed){
				fetch(`${process.env.REACT_APP_API_URL}/orders/create`,{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({orders: orderList.map(order => order.productId)})
				})
				.then(res => res.json())
				.then(result => {
					if (result.status) {
			          Swal.fire({
			            title: 'Order Created!',
			            text: 'Your order has been successfully created.',
			            icon: 'success'
			          });
			          fetchData();
			          setOrderList([]);
			          updateOrderTotal();
			          resetCart();

			        } else {
			          Swal.fire({
			            title: 'Error',
			            text: `${result.message}`,
			            icon: 'error'
			          });
			        }
					
				})
			}
		})
	}

	useEffect(() => {
		if(isCheckoutActive) setIsDisabled(false);
		else setIsDisabled(true);
	}, [isCheckoutActive])

	return (
		<Button className="w-100 banner-button banner-button-custom" disabled={isDisabled} onClick={checkoutOrder}>Checkout</Button>
	)
}