import { Button } from 'react-bootstrap';

export default function QuantityControl({ updateTotalPrice, orderList, fetchData, isCart, product, quantity, setQuantity }) {
  const handleCartQuantity = (newQuantity) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/edit-cart`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        id: product.productId,
        quantity: newQuantity
      })
    })
    .then(res => res.json())
    .then(data => {
      fetchData();
    })
  }

  const handleIncrement = () => {
    if(isCart) handleCartQuantity(quantity + 1);
    else setQuantity(quantity + 1);
    if(orderList !== undefined && orderList.some((orderItem) => orderItem.name === product.name)) {
      orderList.splice(orderList.findIndex((order) => order.name === product.name), 1);
      updateTotalPrice();
    }
    //console.log(orderList);
    
  };

  const handleDecrement = () => {
    if (quantity > 0) {
      if(isCart) handleCartQuantity(quantity - 1);
      else setQuantity(quantity - 1);
      if(orderList !== undefined && orderList.some((orderItem) => orderItem.name === product.name)) {
        orderList.splice(orderList.findIndex((order) => order.name === product.name), 1);
        updateTotalPrice();
      }
      //console.log(orderList);
    }
  };

  return (
    <div className="quantity-control mb-3 text-center">
      <Button className="py-1 px-2 banner-button banner-button-custom " onClick={handleDecrement}>-</Button>
      <span className="mx-3">{quantity}</span>
      <Button className="py-1 px-2 banner-button banner-button-custom " onClick={handleIncrement}>+</Button>
    </div>
  );
}