import {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css';


export default function AppNavbar(){
	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);

	const renderLinks = () => {
        if (user.id !== null) {
            return user.isAdmin ? (
                <>
                    <Nav.Link className="nav-brand" as={NavLink} to="/products">Admin Dashboard</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/logout">Logout</Nav.Link>
                </>
            ) : (
                <>
                    <Nav.Link className="nav-brand" as={NavLink} to="/products">Products</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/profile">Profile</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/cart">Cart</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/orders">Orders</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/logout">Logout</Nav.Link>
                </>
            );
        } else {
            return (
                <>  
                    <Nav.Link className="nav-brand" as={NavLink} to="/products">Products</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/register">Register</Nav.Link>
                    <Nav.Link className="nav-brand" as={NavLink} to="/login">Login</Nav.Link>
                </>
            );
        }
    };

    return (
        <Navbar className="app-navbar bg-dark" expand="lg" fluid="true">
            <Navbar.Brand className="nav-brand me-auto ms-3" as={Link} to="/" expand="xs">The Gadget Place</Navbar.Brand>
            <Navbar.Toggle className="custom-navbar-toggle ms-auto me-3"  aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="ms-3" id="basic-navbar-nav">
                <Nav className="ms-auto">
                    <Nav.Link className="nav-brand" as={NavLink} to="/">Home</Nav.Link>
                    {renderLinks()}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

