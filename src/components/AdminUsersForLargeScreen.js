import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import SetUserToAdmin from './SetUserToAdmin.js';


export default function AdminUsersForLargeScreen({users, fetchData}) {
  const [usersList, setUsersList] = useState([]);

  useEffect(() => {
    const mappedUsers = users.map((user, index) => {
          return (
            <tr key={index}>
              <td className="bg-dark card-text">{user._id}</td>
              <td className="bg-dark card-text">{user.userName}</td>
              <td className="bg-dark card-text">{user.email}</td>
              <td className="bg-dark card-text">{user.mobileNo}</td>
              <td className="bg-dark card-text">
               {(user.isAdmin) ? "Admin" : "User"}
              </td>
              <td className="bg-dark card-text"><SetUserToAdmin user={user} fetchData={fetchData}/></td>
            </tr>
          )
        })
    setUsersList(mappedUsers);
  }, [fetchData, users]);

  return (
    <>
      <Container >
        <div>
          <Table className="table table-striped table-bordered table-responsive ">
            <thead>
              <tr className="text-center">
                <th className="bg-dark card-text">ID</th>
                <th className="bg-dark card-text">Username</th>
                <th className="bg-dark card-text">Email</th>
                <th className="bg-dark card-text">Mobile Number</th>
                <th className="bg-dark card-text">Status</th>
                <th className="bg-dark card-text">Set to Admin</th>
              </tr>
            </thead>
            <tbody className="bg-dark card-text">
              {usersList}
            </tbody>
          </Table>
        </div>
      </Container>
    </>
  );
}
