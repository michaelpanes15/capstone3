import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function RemoveProductFromCart({updateTotalPrice, product, fetchData, orderList}) {

	const removeToCart = (product) => {
		Swal.fire({
	      title: 'Confirm Removal',
	      text: 'Are you sure you want to remove this item from your cart?',
	      icon: 'warning',
	      showCancelButton: true,
	      confirmButtonText: 'Yes, remove it!',
	      cancelButtonText: 'No, keep it',
	    }).then((result) => {
	      if (result.isConfirmed) {
      		fetch(`${process.env.REACT_APP_API_URL}/users//remove-cart/${product.productId}`,{
      			method: 'GET',
      			headers: {
      				'Content-Type': 'application/json',
      				'Authorization': `Bearer ${localStorage.getItem('token')}`
      			}
      		})
      		.then(res => res.json())
      		.then(result => {
      	        //console.log(result);
      			if (result.status) {
      	          Swal.fire({
      	            title: 'Product Removed!',
      	            text: 'Your product has been removed from cart.',
      	            icon: 'success'
      	          });
      	          orderList.splice(orderList.findIndex((order) => order.name === product.name), 1);
      	          updateTotalPrice();
      	          fetchData();

      	        } else {
      	          Swal.fire({
      	            title: 'Error',
      	            text: `${result.message}`,
      	            icon: 'error'
      	          });
      	        }
      			
      		})
	      }
	    });
		
	}

	return(
		<Button className="banner-button banner-button-custom cart-button" size="sm" onClick={() => removeToCart(product)}>Remove from Cart</Button>
	)
}