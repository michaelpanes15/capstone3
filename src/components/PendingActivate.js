import { useState } from 'react';
import { Card, Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function PendingActivate({pendingOrder, fetchData}) {
	const [paymentMethod , setPaymentMethod] = useState('');
	const [deliveryAddress, setDeliveryAddress] = useState('');

	const handlePaymentMethodChange = (e) => {
	  	setPaymentMethod(e.target.value);
	};

	  // Function to handle form submission
	const handleSubmit = async (e) => {
	    e.preventDefault();
	    // Here, you can use the selected paymentMethod and deliveryAddress as needed.
	  	const result = await Swal.fire({
	  	      title: 'Are you sure?',
	  	      text: 'Do you want to finalize your order?',
	  	      icon: 'question',
	  	      showCancelButton: true,
	  	      confirmButtonColor: '#3085d6',
	  	      cancelButtonColor: '#d33',
	  	      confirmButtonText: 'Yes, activate it!',
  	    });

  	    if (result.isConfirmed) {
  	      	// User confirmed, you can proceed with the update action

  	      	fetch(`${process.env.REACT_APP_API_URL}/orders/activate`, {
  	      		method: 'PUT',
  	      		headers: {
  	      			'Content-Type': 'application/json',
  	      			'Authorization': `Bearer ${localStorage.getItem('token')}`
  	      		},
  	      		body: JSON.stringify({
  	      			paymentDetails: paymentMethod,
  	      			deliveryAddress: deliveryAddress
  	      		})
  	      	})
  	      	.then(res => res.json())
  	      	.then(result => {
  	      		if (result.status) {
  	      		  // Order activation was successful, show a success message
      		      Swal.fire({
      		        icon: 'success',
      		        title: 'Order Activated',
      		        text: 'Your order has been successfully activated.',
      		      });

      		      // Perform any other actions or updates if needed
      		      fetchData();
      		    } 
      		    else {
      		      // Order activation failed, show an error message
      		      Swal.fire({
      		        icon: 'error',
      		        title: 'Activation Failed',
      		        text: 'Sorry, there was an error activating your order.',
      		      });
      		    }
  	      	})
  	    }
	};

	const cancelOrder = async () => {
		const result = await Swal.fire({
	  	      title: 'Are you sure?',
	  	      text: 'Do you want to cancel your order?',
	  	      icon: 'warning',
	  	      showCancelButton: true,
	  	      confirmButtonColor: '#3085d6',
	  	      cancelButtonColor: '#d33',
	  	      confirmButtonText: 'Yes, cancel it!',
	  	    });

	  	    if (result.isConfirmed) {
	  	      // User confirmed, you can proceed with the update action
      	      	fetch(`${process.env.REACT_APP_API_URL}/orders/cancel`, {
      	      		method: 'PUT',
      	      		headers: {
      	      			'Content-Type': 'application/json',
      	      			'Authorization': `Bearer ${localStorage.getItem('token')}`
      	      		},
      	      		body: JSON.stringify({orderId: pendingOrder[0]._id})
      	      	})
      	      	.then(res => res.json())
      	      	.then(result => {
      	      		if (result.status) {
      	      		  // Order activation was successful, show a success message
          		      Swal.fire({
          		        icon: 'success',
          		        title: 'Order Cancelled',
          		        text: 'Your order has been cancelled.',
          		      });

          		      // Perform any other actions or updates if needed
          		      fetchData();
          		    } 
          		    else {
          		      // Order activation failed, show an error message
          		      Swal.fire({
          		        icon: 'error',
          		        title: 'Cancellation Failed',
          		        text: 'Sorry, there was an error cancelling your order.',
          		      });
          		    }
      	      	})
	  	    }
	}

	return (
		<Container className="mt-3 px-0 pb-3">
	      <Card className="bg-dark card-text">
	        <Card.Body >
	          <Card.Title>Activate Order</Card.Title>
	          <Form onSubmit={handleSubmit}>
	            <Form.Group>
	              <Form.Label>Payment Method:</Form.Label>
	              <div>
	                <Form.Check
	                  type="radio"
	                  id="credit-card"
	                  name="payment-method"
	                  value="credit-card"
	                  label="Credit Card"
	                  onChange={handlePaymentMethodChange}
	                  checked={paymentMethod === 'credit-card'}
	                />
	                <Form.Check
	                  type="radio"
	                  id="cash"
	                  name="payment-method"
	                  value="cash"
	                  label="Cash"
	                  onChange={handlePaymentMethodChange}
	                  checked={paymentMethod === 'cash'}
	                />
	                <Form.Check
	                  type="radio"
	                  id="GCash"
	                  name="payment-method"
	                  value="GCash"
	                  label="GCash"
	                  onChange={handlePaymentMethodChange}
	                  checked={paymentMethod === 'GCash'}
	                />
	                <Form.Check
	                  type="radio"
	                  id="Paypal"
	                  name="payment-method"
	                  value="Paypal"
	                  label="Paypal"
	                  onChange={handlePaymentMethodChange}
	                  checked={paymentMethod === 'Paypal'}
	                />
	              </div>
	            </Form.Group>
	            <Form.Group className="mb-3">
	              <Form.Label>Delivery Address</Form.Label>
	              <Form.Control
	                type="text"
	                value={deliveryAddress}
	                onChange={(e) => setDeliveryAddress(e.target.value)}
	              />
	            </Form.Group>
	            <Button className="me-3 banner-button banner-button-custom" type="submit" variant="success">
	              Activate Order
	            </Button>
	            <Button variant="danger" onClick={cancelOrder}>
	              Cancel Order
	            </Button>
	          </Form>
	        </Card.Body>
	      </Card>
	    </Container>
	)
}