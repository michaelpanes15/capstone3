import { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap'
import ProductCard from '../components/ProductCard.js';
import '../App.css';

export default function UserView({productsData}){
  const userProducts = productsData;
  const [products, setProducts] = useState([]);
  const [search, setSearch] = useState({
    name: '',
    minPrice: '', 
    maxPrice: ''
  }) 

  const handleNameChange = (event) => {
    const { value } = event.target;
    setSearch((prevData) => ({
      ...prevData,
      "name": (value) ? value : '' ,
    }));
  }

  const handleMinPriceChange = (event) => {
    const { value } = event.target;
    setSearch((prevData) => ({
      ...prevData,
      "minPrice": (value) ? parseFloat(value) : '',
    }));
  }

  const handleMaxPriceChange = (event) => {
    const { value } = event.target;
    setSearch((prevData) => ({
      ...prevData,
      "maxPrice": (value) ? parseFloat(value) : '',
    }));
  }

  const handleSearch = () => {
    try {
      //console.log(search);
      fetch(`${process.env.REACT_APP_API_URL}/products/searchByNameAndPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(search)
      })
      .then(res => res.json())
      .then(result => {
        //console.log(result);
        setProducts(result.message.map((product_item) => {
          return(
            <ProductCard key = {product_item._id} product = {product_item}/>
          )
        }));
      })


    } catch (error) {
      //console.log('Error searching for courses:', error);
    }
  };

  useEffect(() => {
    setProducts(userProducts.filter(product => product.isActive === true).map((product_item) => {
      return(
        <ProductCard key = {product_item._id} product = {product_item}/>
      )
    }));
  }, [userProducts])

  return (
    <>
      <div className="my-3 bg-dark p-2 card-form">
        
        <div className="form-group">
          <div className="input-group mb-2 px-2 py-1">
            <input
              type="text"
              id="courseName"
              className="form-control"
              placeholder="Name"
              value={search.name}
              onChange={handleNameChange}
            />
          </div>
          <div className="input-group mb-2 px-2 py-1">
            <input
              type="number"
              className="form-control"
              id="minPrice"
              placeholder="Minimum Price"
              value={search.minPrice}
              onChange={handleMinPriceChange}
            />
          </div>
          <div className="input-group mb-2 px-2 py-1">
            <input
              type="number"
              className="form-control"
              id="maxPrice"
              placeholder="Maximum Price"
              value={search.maxPrice}
              onChange={handleMaxPriceChange}
            />
          </div>
        </div>
        <button className="btn btn-primary ms-2 banner-button banner-button-custom" onClick={handleSearch}>
          Search
        </button>
      </div>
      
      <Row className="px-2">{products}</Row>
    </>
  )
}