import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import EditProduct from './EditProduct.js';
import ArchiveProduct from './ArchiveProduct.js';

export default function AdminProductsForSmallScreen({productsData, fetchData}) {
  const [product, setProduct] = useState([]);

  useEffect(() => {
    const mappedProduct = productsData.map((product, index) => (
      <Table key={`${product._id}-${index}`} className="table table-striped table-bordered table-responsive">
        <tbody>
          <tr>
            <td className="bg-dark card-text">Product Id</td>
            <td className="bg-dark card-text">{product._id}</td>
          </tr>
          <tr>
            <td className="bg-dark card-text">Name</td>
            <td className="bg-dark card-text">{product.name}</td>
          </tr>
          <tr>
            <td className="bg-dark card-text">Description</td>
            <td className="bg-dark card-text">{product.description}</td>
          </tr>
          <tr>
            <td className="bg-dark card-text">Price</td>
            <td className="bg-dark card-text"><span>&#8369;</span> {product.price}</td>
          </tr>
          <tr>
            <td className="bg-dark card-text">Status</td>
            <td className={(product.isActive) ? "bg-dark card-text" : "text-danger bg-dark card-text"}>{(product.isActive) ? "Available" : "Unavailable"}</td>
          </tr>
          <tr>
            <td className="bg-dark card-text"><EditProduct product={product._id} fetchData={fetchData}/></td>
            <td className="bg-dark card-text"><ArchiveProduct product={product._id} fetchData={fetchData} isActive={(product.isActive)}/></td>
          </tr>
        </tbody>
      </Table>
    ));

    setProduct(mappedProduct);
  }, [productsData, fetchData]);

  return (
    <>
      <Container>
        <div className="d-grid justify-content-center">
          {product}
        </div>
      </Container>
    </>
  );
}
