import {Card, Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import '../App.css';

export default function ProductCard({product}){
  const { name, price, _id, imageUrl } = product;

  return(
    <Col xs={6} md={4} lg={2} className="mx-md-auto px-xs-0 mx-lg-0 px-1">
      <Link to={`/products/${_id}`} className="product-link">
      <Card className="mb-3 bg-dark card-text">
        <Card.Img className="p-1 img-fluid w-100 mx-auto" variant="top" src={imageUrl} />
        <Card.Body>
          <Card.Title className="truncate-text-two-lines">{name}</Card.Title>
          <Card.Text><span>&#8369;</span> {price}</Card.Text>
        </Card.Body>
      </Card>
      </Link>
    </Col>
  )
}

// PropTypes for data validation
ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  }).isRequired 
}