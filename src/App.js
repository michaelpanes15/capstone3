import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar.js';
import Cart from './pages/Cart.js';
import Products from './pages/Products.js';
import ProductView from './pages/ProductView.js';
import Error from './pages/Error.js'
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import Orders from './pages/Orders.js';
import Profile from './pages/Profile.js';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])

    // Efficient hot reload
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }})
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(typeof data.message._id !== "undefined"){
                setUser({
                    id: data.message._id,
                    isAdmin: data.message.isAdmin
                })
            }
            else {
                setUser({
                    id: null,
                    isAdmin: null
                });
            }
        })
    }, [])

    return (
        <UserProvider value={{user, setUser, unsetUser }}>
            <Router>
                <Container className="p-0" fluid><AppNavbar/></Container>
                <Container className="">
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/products" element={<Products />} />
                        <Route path="/products/:productId" element={<ProductView />} />
                        <Route path="/cart" element={<Cart />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="/login" element={<Login />} /> 
                        <Route path="/logout" element={<Logout />} />
                        <Route path="/orders" element={<Orders />} />
                        <Route path="/profile" element={<Profile />} />
                        <Route path="*" element={<Error />} />
                    </Routes>
                </Container>
            </Router>
        </UserProvider>
        
  );
}

export default App;
