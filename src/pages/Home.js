import Banner from '../components/Banner.js';
import FeaturedProducts from '../components/FeaturedProducts.js';

export default function Home(){
	const data = {
		title: "The Gadget Place",
		description: "Your One-Stop Shop to all your gadget needs!",
		destination: "/products",
		label: "Buy now!"
	}

	return (
		<>
			<Banner data={data} />
			<FeaturedProducts/>
		</>
	)
}