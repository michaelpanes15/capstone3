import { useState, useEffect, useContext, useCallback } from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import QuantityControl from '../components/QuantityControl.js';
import AddCartToOrder from '../components/AddCartToOrder.js';
import CheckoutOrder from '../components/CheckoutOrder.js';
import RemoveProductFromCart from '../components/RemoveProductFromCart.js'
import '../App.css'

export default function CartView() {
  const {user} = useContext(UserContext);
  const [cart, setCart] = useState([]);
  const [mappedCart, setMappedCart] = useState([]);
  const [orderList, setOrderList] = useState([]);
  const [total, setTotal] = useState(0);
  const [isCheckoutActive, setIsCheckoutActive] = useState(false);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/get-cart`,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(result => {
      if(result.status){
        setCart(result.data);  
      }
    })
  };

  const updateOrderTotal = useCallback(() => {
    if(orderList.length > 0){
      let totalPriceList = orderList.map((order) => order.subTotal);
      setTotal(totalPriceList.reduce((x, y) => x+y), 0);
    }
    else setTotal(0);
  }, [orderList])

  const resetCart = () => {
    setTotal(0);
  }

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setMappedCart(cart.map((product) => (
      <Col lg={{ span: 8, offset: 2 }} key={product._id} >
        <Card className="bg-dark card-text mb-2"  >
          <Card.Body >
            <Row className="align-items-center justify-content-center justify-content-md-start">
              <Col xs={{ span: 'auto', order: 'last' }} md={{ span: 6, order: 'first' }} lg={{ span: 4, offset: 2 }}>
                <div className="text-center py-2">
                  <Card.Title>{product.name}</Card.Title>
                  <QuantityControl
                    updateTotalPrice={updateOrderTotal}
                    orderList={orderList}
                    fetchData={fetchData}
                    isCart={true}
                    product={product}
                    quantity={product.quantity}
                  />
                  <Card.Text>
                    Subtotal: <span>&#8369;</span> {product.subTotal}
                  </Card.Text>
                  <div className="d-flex justify-content-center">
                    <Card.Text className="me-2">
                      <AddCartToOrder
                        updateTotalPrice={updateOrderTotal}
                        orderList={orderList}
                        product={product}
                        fetchData={fetchData}
                        totalAmount={total}
                      />
                    </Card.Text>
                    <Card.Text className="me-2">
                      <RemoveProductFromCart
                        updateTotalPrice={updateOrderTotal}
                        orderList={orderList}
                        product={product}
                        fetchData={fetchData}
                        totalAmount={total}
                      />
                    </Card.Text>
                  </div>
                </div>
              </Col>
              <Col xs={{ span: 'auto', order: 'first' }} md={{ span: 6, order: 'last' }} lg={5}>
                <Card.Img
                  className="p-1 img-fluid"
                  variant="top"
                  src={product.imageUrl}
                />
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
    )))
  }, [cart, orderList, total, updateOrderTotal])

  useEffect(() => {
    //console.log(total);
    if(total > 0) setIsCheckoutActive(true);
    else setIsCheckoutActive(false);
  }, [total, setIsCheckoutActive])

  return (
    (user.isAdmin) ?
    <Navigate to="/products" />
    :
    (cart.length === 0) ?
        <div className="text-center mt-3 mb-3">
            <p className="fs-5 header-title">No Products in Cart</p>
          </div>
    :
    <>
      <div className="my-3">
        <div className="mb-5 pb-5"  >
          {mappedCart}
        </div>
        <div className= "checkout d-flex justify-content-center mt-3 p-0 sticky-footer">
          <Row className="py-2 w-100 bg-dark">
            <Col xs={7} md={10} className="p-0 ps-2">
              <CheckoutOrder isCheckoutActive={isCheckoutActive} orderList={orderList} setOrderList={setOrderList} total={total} fetchData={fetchData} updateOrderTotal={updateOrderTotal} resetCart={resetCart} />
            </Col>
            <Col xs={5} md={2} className="me-auto p-0 ps-1 cart-total">
              Total: <span>&#8369;</span> {total}
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}
