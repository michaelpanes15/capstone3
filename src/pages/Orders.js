import { useState, useContext, useEffect } from 'react';
import {Navigate} from 'react-router-dom';
import OrdersNavbar from '../components/OrdersNavbar';
import PendingOrdersView from '../components/PendingOrdersView';
import ActiveOrdersView from '../components/ActiveOrdersView';
import ClosedOrdersView from '../components/ClosedOrdersView';
import UserContext from '../UserContext.js';

export default function Orders() {
	const {user} = useContext(UserContext);
	const [isPending, setIsPending] = useState(false);
  	const [isActive, setIsActive] = useState(false);
  	const [isClosed, setIsClosed] = useState(true);
  	const [orders, setOrders] = useState([]);

  	const fetchData = () => {
  		fetch(`${process.env.REACT_APP_API_URL}/orders/user/all`, {
  			method: 'GET',
  			headers: {
  				'Content-Type': 'application/json',
  				'Authorization': `Bearer ${localStorage.getItem('token')}`
  			}
  		})
  		.then(res => res.json())
  		.then(result => {
  			if(result.status){
  				//console.log(result.message);
  				setOrders(result.message);
  			}
  		})
  	}

  	useEffect(() => {
  		fetchData();
  		//console.log(orders);
  	}, [orders])

	return (
		(user.isAdmin)?
		<Navigate to='/courses' />
		:
		<>
			<OrdersNavbar isPending={isPending} setIsPending={setIsPending} isActive={isActive} setIsActive={setIsActive} isClosed={isClosed} setIsClosed={setIsClosed}/>
			<PendingOrdersView isPending={isPending} orders={orders.filter((order) => order.orderStatus === "Pending")} fetchData={fetchData} />
			<ActiveOrdersView isActive={isActive} orders={orders.filter((order) => order.orderStatus === "Active")} fetchData={fetchData} />
			<ClosedOrdersView isClosed={isClosed} orders={orders.filter((order) => order.orderStatus === "Closed")} fetchData={fetchData} />
		</>
	)
}