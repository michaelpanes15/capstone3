import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../App.css'

export default function Register() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [userName, setUserName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const registerUser = (event) => {
		//prevents page reloading
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userName: userName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(response => response.json()).then(result => {

			if(result.status) {
				setUserName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setConfirmPassword("");
				Swal.fire({
					title: "Register success!",
					icon: "success",
					text: "Welcome!"
				})
				navigate("/login");
			}
			else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: `${result.message}`
				})
			}
			
		})
	}

	useEffect(() => {
		if((userName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") 
			&& (confirmPassword === password) && (mobileNo.length === 11) && (email.includes("@"))) setIsActive(true);
		else setIsActive(false);
	}, [userName, email, mobileNo, password, confirmPassword]);

	return(
		<div>
	      {user.id !== null ? (
	        <Navigate to="/products" />
	      ) : (
	        <div className="bg-dark p-3 my-3 card-form">
	          <Form onSubmit={(event) => registerUser(event)}>
	            <h1 className="my-5 text-center header-title">Register</h1>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Username:</Form.Label>
	              <Form.Control
	                type="text"
	                placeholder="Enter Username"
	                value={userName}
	                onChange={(event) => {
	                  setUserName(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Email:</Form.Label>
	              <Form.Control
	                type="email"
	                placeholder="Enter Email"
	                value={email}
	                onChange={(event) => {
	                  setEmail(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Mobile No:</Form.Label>
	              <Form.Control
	                type="number"
	                placeholder="Enter Mobile No."
	                value={mobileNo}
	                onChange={(event) => {
	                  setMobileNo(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Password:</Form.Label>
	              <Form.Control
	                type="password"
	                placeholder="Enter Password"
	                value={password}
	                onChange={(event) => {
	                  setPassword(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Confirm Password:</Form.Label>
	              <Form.Control
	                type="password"
	                placeholder="Confirm Password"
	                value={confirmPassword}
	                onChange={(event) => {
	                  setConfirmPassword(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <div className="text-center"> {/* Center-align content */}
                  <Button className="banner-button banner-button-custom" variant="primary" type="submit" disabled={isActive === false}>
                    Register
                  </Button>
                </div>
	          </Form>
	          <p className="mt-3 text-center header-title">
	            Already have an account?{' '}
	            <Link to="/login" className="header-title">Login now</Link> {/* Add this link */}
	          </p>
	        </div>
	      )}
	    </div>
	)
}