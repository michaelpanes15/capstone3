import {useState, useEffect, useContext, useCallback} from 'react';
import {Card} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import ResetPassword from '../components/ResetPassword.js';
import UpdateProfile from '../components/UpdateProfile.js';
import UserContext from '../UserContext';


export default function Profile() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [userName, setUserName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	
	const fetchData = useCallback(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		    method: 'GET',
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }})
		.then(response => response.json())
		.then(data => {
		    //console.log(data);
		    if(typeof data.message._id !== "undefined"){
		        setUserName(data.message.userName);
		        setMobileNo(data.message.mobileNo);
		        setEmail(data.message.email);
		    }
		    else {
		    	navigate("/");
		    }
		})
	}, [navigate])

	useEffect(() => {
		fetchData();
	}, [fetchData])

	return(
		(user.id === null) ?
			<Navigate to="/" />
		:
		<>
			<Card className="my-3 bg-dark card-text">
			  <Card.Body>
			    <Card.Title className="p-3 header-title"><h1>Profile</h1></Card.Title>
			    <Card.Title className="border-bottom p-3 header-title"><h1>{userName}</h1></Card.Title>
			    
			    <Card.Subtitle className="pt-3 px-3"><h4>Contacts:</h4></Card.Subtitle>
			    	<ul className="px-4 mx-3">
			    		<li>Email: {email}</li>
			    		<li>Mobile No: {mobileNo}</li>
			    	</ul>
			  </Card.Body>
			</Card>	

			<Card className="mb-3 bg-dark card-text">
			  <Card.Body>
			    <ResetPassword user={user}/>
			  </Card.Body>
			</Card>	

			<Card className="mb-3 bg-dark card-text">
			  <Card.Body>
			    <UpdateProfile user={user} fetchData={fetchData}/>
			  </Card.Body>
			</Card>	
		</>
	)
}