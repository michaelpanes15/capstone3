import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import QuantityControl from '../components/QuantityControl';

export default function ProductView() {
	const {user} = useContext(UserContext);
	const {productId} = useParams();
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [imageUrl, setImageUrl] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const [isDisabled, setIsDisabled] = useState(true);

	const addToCart = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/addToCart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			if(data.status === true) {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have successfully added to cart!"
				})

				// Redirects back to /courses
				navigate("/products");
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: `${data.message.message}`
				})
			}
			//console.log(data);
		})
	};

	useEffect(() => {
		//console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImageUrl(data.imageUrl)
		})
	}, [productId]);

	useEffect(() => {
		if(quantity > 0) setIsDisabled(false);
		else setIsDisabled(true);
	}, [quantity])

	return(
		(user.isAdmin)?
		<Navigate to="/products" />
		:
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card className="mb-3 bg-dark card-text">
					  <Card.Img className="p-3 img-fluid w-100 mx-auto" variant="top" src={imageUrl} />
					  <Card.Body className="text-center">
					    <Card.Title>{name}</Card.Title>
					    
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text> 

					    
					    <Card.Text><span>&#8369;</span> {price}</Card.Text> 
					    <QuantityControl isCart={false} quantity={quantity} setQuantity={setQuantity} />
				      	{
				      		(user.id !== null) ? 
				      			<Button className="banner-button banner-button-custom" disabled={isDisabled} onClick={() => addToCart(productId)}>Add to Cart</Button>
				      		:
				      			<Link className="btn btn-danger" to="/login">Login to Buy</Link>
				      	}
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
	)
}