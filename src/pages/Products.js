import {useEffect, useState, useContext} from 'react';
import UserView from '../components/UserView.js'
import AdminView from '../components/AdminView.js';
import UserContext from '../UserContext';

export default function Products() {

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext)

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			setProducts(data);
		})
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		(user.isAdmin) ?
			<AdminView productsData={products} fetchData={fetchData} isAdmin={user.isAdmin}/>
		:
			<UserView productsData={products}/>
		
	)
}