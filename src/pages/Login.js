import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

	// Allows to consume the User context object and its properties to use for use validation
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true);
	const navigate = useNavigate();
	const authenticate = (event) => {
		//prevents page reloading
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response => response.json()).then(result => {
			//console.log(result.accessToken);
			if(result.accessToken) {
				localStorage.setItem('token', result.accessToken);
				retrieveUserDetails(result.accessToken);
				// setUser({
				// 	accessToken: localStorage.getItem('token')
				// })
				Swal.fire({
					title: "Login successful",
					icon: "success",
					text: "Welcome!"
				})
				navigate('/products');
			}
			else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})
		setEmail("");
		setPassword("");
	};

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'GET',
			headers: {
				'Authorization': `Bearer ${token}`
			}})
		.then(response => response.json())
		.then(data => {
			//console.log(data);
			setUser({
				id: data.message._id,
				isAdmin: data.message.isAdmin
			})
		})
	};


	useEffect(() => {
		if((email !== "" && password !== "")) setIsActive(false);
		else setIsActive(true);
	}, [email, password]);

	return(
		<div>
	      {user.id !== null ? (
	        <Navigate to="/products" />
	      ) : (
	        <div className="bg-dark p-3 my-3 card-form">
	          <Form onSubmit={(event) => authenticate(event)}>
	            <h1 className="my-5 text-center header-title">Login</h1>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Email:</Form.Label>
	              <Form.Control
	                type="email"
	                placeholder="Enter Email"
	                value={email}
	                onChange={(event) => {
	                  setEmail(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <Form.Group className="mb-3">
	              <Form.Label className="header-title">Password:</Form.Label>
	              <Form.Control
	                type="password"
	                placeholder="Enter Password"
	                value={password}
	                onChange={(event) => {
	                  setPassword(event.target.value);
	                }}
	                required
	              />
	            </Form.Group>
	            <div className="text-center"> {/* Center-align content */}
                  <Button className="banner-button banner-button-custom" variant="primary" type="submit" disabled={isActive === true}>
                    Login
                  </Button>
                </div>
	          </Form>

	          <p className="mt-3 text-center header-title">
	            Do not have an account?{' '}
	            <Link to="/register" className="header-title">Register now</Link> {/* Add this link */}
	          </p>
	        </div>
	      )}
	    </div>
	)
}